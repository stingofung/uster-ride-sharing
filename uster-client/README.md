# Project for Uster Technologies Inc. 

A Spring MVC web applications. It follows the Model-View-Controller design pattern. It implements all the basic features of a core spring framework like Inversion of Control, Dependency Injection.