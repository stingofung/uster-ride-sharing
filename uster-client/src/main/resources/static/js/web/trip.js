var recordType = "trips";
var tableList;
var sudoNotify = $('.notification-container').sudoNotify();
var stepper;
var stepper1;
overlayLoading();

$(document).ready(function() {
    recordLoad();
});

function recordLoad() {
    $('#' + recordType + '-list').DataTable().destroy();
    $('.recordAction').html('Load ')

    tableList = $('#' + recordType + '-list').DataTable({
        "processing": true,
        "serverSide": true,
        "scrollX": true,
        "ajax": {
            "url": "/trips2",
            "type": getRequestMethod(),
            "dataType": "json",
            "contentType": "application/json",
            "data": function (d) {
                return JSON.stringify(d);
            }
        },
        "drawCallback": function (settings) {
            if (settings.jqXHR != null && settings.jqXHR.readyState == 4) {
                commonActions('RemoveOverlay', '', '');
            }
        },
        "columns": [
            {"data": "vehicleId", "width": "auto", "class": "hidden"},
            {"data": "vehicleBrand", "width": "auto"},
            {"data": "vehicleModel", "width": "auto"},
            {"data": "vehiclePlate", "width": "auto"},
            {"data": "vehicleLicense", "width": "auto"},
            {"data": "driverId", "width": "auto", "class": "hidden"},
            {"data": "driverName", "width": "auto"},
            {"data": "driverSurname", "width": "auto"},
            {"data": "driverLicense", "width": "auto"},
            {
                "data": "date",
                "width": "auto",
                "render": function (data, type, full, meta) {
                    return "<div class='date'>" + setFormatDate(full["date"], 1) + "<div>";
                }
            },
        ],
        "columnDefs": [
            /*{ className: "dt-head-center", targets: [ 2 ] },*/
            { className: "dt-body-center", targets: [ 4, 8] },
            {
                "targets": -1,
                "orderable": false,
                "data": null
            }
        ]
    });

    $('#' + recordType + '-list').css('display','table');
}

function loadVehicles() {
    overlayLoading();
    var data = {};
    data.date = setFormatDate($("#txt_Date").val(), 2);

    $.ajax({
        type: 'POST',
        url: '/vehicles2/checkAvailabilityByDate',
        data: JSON.stringify(data),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (response) {
            $("#slc_VehicleId").empty();
            if (response.length > 0) {
                $('#slc_VehicleId').append('<option value="0">- select option -</option>');
                $.each(response, function (i, item) {
                    $('#slc_VehicleId').append('<option value="' + item.id + '" data-prefix-brand="' + item.brand + '" data-prefix-model="' + item.model + '" data-prefix-plate="' + item.plate + '" data-prefix-license="' + item.license + '">' + item.brand + ' ' + item.model + ' -> ' + item.plate + ' (License Required: ' + item.license + ')</option>');
                });
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
        }
    });

    commonActions('RemoveOverlay', '', '');
}

function loadDrivers() {
    overlayLoading();
    var data = {};
    data.date = setFormatDate($("#txt_Date").val(), 2);
    data.vehicleLicense = $("#slc_VehicleId").find(':selected').attr('data-prefix-license');

    $.ajax({
        type: 'POST',
        url: '/drivers2/checkAvailabilityByDateAndLicense',
        data: JSON.stringify(data),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (response) {
            $("#slc_DriverId").empty();
            if (response.length > 0) {
                $('#slc_DriverId').append('<option value="0">- select option -</option>');
                $.each(response, function (i, item) {
                    $('#slc_DriverId').append('<option value="' + item.id + '" data-prefix-name="' + item.name + '" data-prefix-surName="' + item.surName + '" data-prefix-license="' + item.license + '">' + item.name + ' ' + item.surName + ' (License: ' + item.license + ')</option>');
                });
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
        }
    });

    commonActions('RemoveOverlay', '', '');
}

function recordNew() {
    newRecordStyleConfiguration(recordType);
    initializeForm();
    initializeSteppers();
    commonActions('DisplayPanel', 'dv_Form', '');
};

function saveAll() {
    overlayLoading();
    var data = {};
    data.date = setFormatDate($('#txt_Date').val(), 2);
    data.vehicleId = $('#slc_VehicleId').val();
    data.driverId = $('#slc_DriverId').val();
    saveAjaxCall(data, 'saved', recordType);
};

function initializeForm() {
    initializeDateTimePicker('txt_Date');
    $('#hdf_Id').val(0);
    $('#slc_VehicleId').empty();
    $('#slc_DriverId').empty();
};

function initializeSteppers() {
    stepper = new Stepper($('.bs-stepper')[0]);
    stepper1 = new Stepper(document.querySelector('#stepper1'));
};

function validateLoadDrivers() {
    if ($('#slc_VehicleId').val() > 0) {
        return true;
    } else {
        getMessage(-2, 'You must specified the Vehicle to continue.');
    }
};

function validateSave() {
    if ($('#slc_DriverId').val() > 0) {
        return true;
    } else {
        getMessage(-2, 'You must specified the Driver to continue.');
    }
};