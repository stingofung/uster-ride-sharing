package com.hiberus.uster.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Trip {

    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("date")
    private LocalDate date;

    private Long vehicleId;
    private String vehicleBrand;
    private String vehicleModel;
    private String vehiclePlate;
    private String vehicleLicense;

    private Long driverId;
    private String driverName;
    private String driverSurname;
    private String driverLicense;

    @Override
    public String toString() {
        return "{\"vehicleId\":" + vehicleId + ", \"driverId\":" + driverId + ", \"date\":" + date + "}";
    }

}