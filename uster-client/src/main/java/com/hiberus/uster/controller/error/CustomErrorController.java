package com.hiberus.uster.controller.error;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomErrorController implements ErrorController {

    public CustomErrorController() {}

    @RequestMapping(value = "/error")
    public String handleError(HttpServletRequest request)  {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        /*Object uri = request.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI);
        String callerURI = uri.toString();
        callerURI = callerURI.substring(0, callerURI.lastIndexOf("/")).replace("2", "").replace("/", "");*/

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "not-found";
            }
        }

        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

}