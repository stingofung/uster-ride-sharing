package com.hiberus.uster.controller;

import com.hiberus.uster.model.Driver;
import com.hiberus.uster.model.Trip;
import com.hiberus.uster.model.paging.Page;
import com.hiberus.uster.model.paging.PagingRequest;
import com.hiberus.uster.service.DriverService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/drivers2")
public class DriverController {

    private final DriverService driverService;

    @Autowired
    public DriverController(DriverService driverService) {
        this.driverService = driverService;
    }

    @PostMapping
    public Page<Driver> getRecord(@RequestBody PagingRequest pagingRequest) {
        log.info("[DriverController (CLIENT)] GET - Request received: /drivers");
        return driverService.getDrivers(pagingRequest);
    }

    @RequestMapping(value = "/checkAvailabilityByDateAndLicense", method = RequestMethod.POST)
    public @ResponseBody Driver[] checkAvailabilityByDateAndLicense(@RequestBody Trip trip, HttpServletRequest request, HttpServletResponse response) {
        log.info("[DriverController (CLIENT)] POST - Request received: /drivers/checkAvailabilityByDateAndLicense");
        return driverService.getDriversAvailabilityByDateAndLicense(trip, request, response);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addRecord(@RequestBody Driver driver) {
        log.info("[DriverController (CLIENT)] POST - Request received: /drivers/add -> " + driver.toString());
        driverService.addDriver(driver);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void updateRecord(@PathVariable(value = "id") Long id, @RequestBody Driver driver) {
        log.info("[DriverController (CLIENT)] PUT - Request received: /drivers/" + id + " -> " + driver.toString());
        driverService.updateDriver(id, driver);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRecord(@PathVariable(value = "id") Long id) {
        log.info("[DriverController (CLIENT)] DELETE - Request received: /drivers/" + id);
        driverService.deleteDriver(id);
    }

}