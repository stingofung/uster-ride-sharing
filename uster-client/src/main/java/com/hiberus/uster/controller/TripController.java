package com.hiberus.uster.controller;

import com.hiberus.uster.model.Trip;
import com.hiberus.uster.model.paging.Page;
import com.hiberus.uster.model.paging.PagingRequest;
import com.hiberus.uster.service.TripService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/trips2")
public class TripController {

    private final TripService tripService;

    @Autowired
    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @PostMapping
    public Page<Trip> getRecord(@RequestBody PagingRequest pagingRequest) {
        log.info("[TripController (CLIENT)] GET - Request received: /trips");
        return tripService.getTrips(pagingRequest);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addRecord(@RequestBody Trip trip) {
        log.info("[TripController (CLIENT)] POST - Request received: /trips/add -> " + trip.toString());
        tripService.addTrip(trip);
    }

}