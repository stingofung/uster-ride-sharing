# USTER Ride Sharing 

The idea: Open an app, designate your pickup location, and summon a driver who will drive you to your destination

This project is composed with two modules:

- uster-api
- uster-client

The technologies used for the development of the project were:

- Spring MVC
- Spring Boot 
- Bootstrap + Thymeleaf
- H2 (DBMS)
- JPA
- JDK 1.8
- Maven
- Swagger
- IDE - IntelliJ
- jQuery

## USTER API

RESTful API that uses HTTP requests to **GET**, **PUT**, **POST** and **DELETE** data.

## USTER Client

A Spring MVC web applications. It follows the Model-View-Controller design pattern. It implements all the basic features of a core spring framework like Inversion of Control, Dependency Injection.