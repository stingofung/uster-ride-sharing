package com.hiberus.uster.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "vehicles")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name = "brand", nullable = false)
    private String brand;
    @Column(name = "model", nullable = false)
    private String model;
    @Column(name = "plate", nullable = false)
    private String plate;
    @Column(name = "license", nullable = false)
    @Length(min = 1, max = 1)
    private String license;

    /*@OneToOne(mappedBy = "vehicle", fetch = FetchType.LAZY)
    private Trip trip;*/

}
