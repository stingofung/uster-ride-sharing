package com.hiberus.uster.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "drivers")
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "surName", nullable = false)
    private String surName;
    @Column(name = "license", nullable = false)
    @Length(min = 1, max = 1)
    private String license;

    /*@OneToOne(mappedBy = "driver", fetch = FetchType.LAZY)
    private Trip trip;*/

}