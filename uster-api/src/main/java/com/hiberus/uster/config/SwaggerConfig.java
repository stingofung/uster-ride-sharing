package com.hiberus.uster.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${expanded.project.name}")
    private String projectName;
    @Value("${expanded.project.version}")
    private String projectVersion;
    @Value("${expanded.project.description}")
    private String projectDescription;
    @Value("${custom.project.license}")
    private String projectLicense;
    @Value("${custom.project.licenseUrl}")
    private String projectLicenseURL;
    @Value("${custom.project.contactName}")
    private String projectContactName;
    @Value("${custom.project.contactEmail}")
    private String projectContactEmail;

    private static final String CONTROLLER_PACKAGE = "com.hiberus.uster.controller";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(CONTROLLER_PACKAGE))
                .paths(PathSelectors.any()).build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(projectName)
                .description("\"" + projectDescription + "\"").
                        version(projectVersion)
                .license(projectLicense)
                .licenseUrl(projectLicenseURL)
                .contact(new Contact(projectContactName, "contactURL", projectContactEmail))
                .build();
    }

}