package com.hiberus.uster.exception;

@SuppressWarnings("serial")
public class NotFoundException extends RuntimeException {

    private boolean hasCode;

    public NotFoundException() {
        super("Entity not found");
    }

    public NotFoundException(String code) {
        super(code);
        hasCode = true;
    }

    public boolean hasCode(){
        return hasCode;
    }

}