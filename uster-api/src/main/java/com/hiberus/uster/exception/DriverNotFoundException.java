package com.hiberus.uster.exception;

public class DriverNotFoundException extends RuntimeException {

    private boolean hasCode;

    public DriverNotFoundException() {
        super("Entity not found");
    }

    public DriverNotFoundException(String code) {
        super(code);
        hasCode = true;
    }

    public boolean hasCode(){
        return hasCode;
    }

}