package com.hiberus.uster.exception;

import lombok.Getter;

import org.springframework.validation.Errors;

@SuppressWarnings("serial")
@Getter
public class BadRequestException extends RuntimeException {

    private Errors errors;

    public BadRequestException(Errors errors) {
        super("Bad request");
        this.errors = errors;
    }

}