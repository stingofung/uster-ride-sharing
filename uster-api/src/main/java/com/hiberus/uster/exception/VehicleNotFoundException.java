package com.hiberus.uster.exception;

public class VehicleNotFoundException extends RuntimeException {

    private boolean hasCode;

    public VehicleNotFoundException() {
        super("Entity not found");
    }

    public VehicleNotFoundException(String code) {
        super(code);
        hasCode = true;
    }

    public boolean hasCode(){
        return hasCode;
    }

}
