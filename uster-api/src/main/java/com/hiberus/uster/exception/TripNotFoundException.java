package com.hiberus.uster.exception;

public class TripNotFoundException extends NotFoundException {

    private boolean hasCode;

    public TripNotFoundException() {
        super("Entity not found");
    }

    public TripNotFoundException(String code) {
        super(code);
        hasCode = true;
    }

    public boolean hasCode(){
        return hasCode;
    }

}


