package com.hiberus.uster.validation;

import com.hiberus.uster.dto.VehicleDTO;
import com.hiberus.uster.entity.Vehicle;
import com.hiberus.uster.repository.VehicleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

@Component
public class VehicleValidator implements Validator {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return VehicleDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        VehicleDTO vehicleDTO = (VehicleDTO) object;
        Optional<Vehicle> tmpVehicle;

        if (vehicleDTO.getId() == null) {
            tmpVehicle = vehicleRepository.findByBrandAndModelAndPlateAndLicense(vehicleDTO.getBrand(), vehicleDTO.getModel(), vehicleDTO.getPlate(), vehicleDTO.getLicense());
            if (tmpVehicle.isPresent()) {
                errors.rejectValue("id", "validation.vehicle.alreadyExist");
            }
        }
    }
}