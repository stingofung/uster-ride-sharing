package com.hiberus.uster.validation;

import com.hiberus.uster.dto.TripDTO;
import com.hiberus.uster.entity.Trip;
import com.hiberus.uster.repository.TripRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

@Component
public class TripValidator implements Validator {

    @Autowired
    private TripRepository tripRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return TripDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        TripDTO tripDTO = (TripDTO) object;
        Optional<Trip> tmpTrip;

        if (tripDTO.getId() == null) {
            tmpTrip = tripRepository.findByDateAndVehicleAndDriver(tripDTO.getDate(), tripDTO.getVehicleId(), tripDTO.getDriverId());
            if (tmpTrip.isPresent()) {
                errors.rejectValue("id", "validation.trip.alreadyExist");
            }
        }
    }

}