package com.hiberus.uster.validation;

import com.hiberus.uster.dto.DriverDTO;
import com.hiberus.uster.entity.Driver;
import com.hiberus.uster.repository.DriverRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

@Component
public class DriverValidator implements Validator {

    @Autowired
    private DriverRepository driverRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return DriverDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        DriverDTO driverDTO = (DriverDTO) object;
        Optional<Driver> tmpDriver;

        if (driverDTO.getId() == null) {
            tmpDriver = driverRepository.findByNameAndSurNameAndLicense(driverDTO.getName(), driverDTO.getSurName(), driverDTO.getLicense());
            if (tmpDriver.isPresent()) {
                errors.rejectValue("id", "validation.driver.alreadyExist");
            }
        }
    }
}