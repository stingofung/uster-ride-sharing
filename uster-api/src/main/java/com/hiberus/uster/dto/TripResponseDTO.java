package com.hiberus.uster.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hiberus.uster.entity.Trip;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;

@ApiModel(description = "All details about the Trip")
public class TripResponseDTO {

    @ApiModelProperty(notes = "The database generated trip ID")
    private Long id;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("date")
    @ApiModelProperty(notes = "The trip date", dataType = "java.lang.String", example = "yyyy-MM-dd")
    private LocalDate date;
    @ApiModelProperty(notes = "The trip vehicleID")
    private Long vehicleId;
    @ApiModelProperty(notes = "The vehicle brand")
    private String vehicleBrand;
    @ApiModelProperty(notes = "The vehicle model")
    private String vehicleModel;
    @ApiModelProperty(notes = "The vehicle plate")
    private String vehiclePlate;
    @ApiModelProperty(notes = "The vehicle license")
    private String vehicleLicense;
    @ApiModelProperty(notes = "The trip driverID")
    private Long driverId;
    @ApiModelProperty(notes = "The driver name")
    private String driverName;
    @ApiModelProperty(notes = "The driver surname")
    private String driverSurname;
    @ApiModelProperty(notes = "The driver type license")
    private String driverLicense;

    public TripResponseDTO(){

    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getVehicleId() {
        return vehicleId;
    }
    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleBrand() {
        return vehicleBrand;
    }
    public void setVehicleBrand(String vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }
    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehiclePlate() {
        return vehiclePlate;
    }
    public void setVehiclePlate(String vehiclePlate) {
        this.vehiclePlate = vehiclePlate;
    }

    public String getVehicleLicense() {
        return vehicleLicense;
    }
    public void setVehicleLicense(String vehicleLicense) {
        this.vehicleLicense = vehicleLicense;
    }

    public Long getDriverId() {
        return driverId;
    }
    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverSurname() {
        return driverSurname;
    }
    public void setDriverSurname(String driverSurname) {
        this.driverSurname = driverSurname;
    }

    public String getDriverLicense() {
        return driverLicense;
    }
    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }

    public static TripResponseDTO fromTrip(Trip trip) {
        TripResponseDTO dto = new TripResponseDTO();
        dto.setId(trip.getId());
        dto.setDate(trip.getDate());
        dto.setVehicleId(trip.getVehicle().getId());
        dto.setVehicleBrand(trip.getVehicle().getBrand());
        dto.setVehicleModel(trip.getVehicle().getModel());
        dto.setVehiclePlate(trip.getVehicle().getPlate());
        dto.setVehicleLicense(trip.getVehicle().getLicense());
        dto.setDriverId(trip.getDriver().getId());
        dto.setDriverName(trip.getDriver().getName());
        dto.setDriverSurname(trip.getDriver().getSurName());
        dto.setDriverLicense(trip.getDriver().getLicense());
        return dto;
    }

}