package com.hiberus.uster.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hiberus.uster.entity.Vehicle;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@ApiModel(description = "All details about the Vehicle")
public class VehicleDTO {

    @ApiModelProperty(notes = "The database generated vehicle ID")
    @JsonIgnore
    private Long id;

    @ApiModelProperty(notes = "The vehicle brand", example = "Chevrolet")
    @NotBlank(message = "{validation.vehicle.brand.notBlank}")
    private String brand;

    @ApiModelProperty(notes = "The vehicle model", example = "Camaro")
    @NotBlank(message = "{validation.vehicle.model.notBlank}")
    private String model;

    @ApiModelProperty(notes = "The vehicle plate", example = "ABC-123")
    @NotBlank(message = "{validation.vehicle.plate.notBlank}")
    private String plate;

    @ApiModelProperty(notes = "The vehicle license", example = "A")
    @NotBlank(message = "{validation.vehicle.license.notBlank}")
    @Size(min = 1, max = 1, message = "{validation.vehicle.license.size}")
    private String license;

    public VehicleDTO() {

    }

    public VehicleDTO(Vehicle vehicle) {
        this.id = vehicle.getId();
        this.brand = vehicle.getBrand();
        this.model = vehicle.getModel();
        this.plate = vehicle.getPlate();
        this.license = vehicle.getLicense();
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public String getPlate() {
        return plate;
    }
    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getLicense() {
        return license;
    }
    public void setLicense(String license) {
        this.license = license;
    }

    @Override
    public String toString() {
        return "{\"brand\":" + brand + ", \"model\":" + model + ", \"plate\":" + plate + ", \"license\":" + license + "}";
    }

}