package com.hiberus.uster.dto.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JsonErrorDTO {

    private String message;
    private List<Error> errors = new ArrayList<>();

    public JsonErrorDTO(String message) {
        this.message = message;
    }

    public void addError(String field, String message) {
        errors.add(new Error(field, message));
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Error {
        private String field;
        private String message;
    }

}