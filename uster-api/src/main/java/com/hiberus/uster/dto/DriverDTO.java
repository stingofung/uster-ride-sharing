package com.hiberus.uster.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hiberus.uster.entity.Driver;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@ApiModel(description = "All details about the Driver")
public class DriverDTO {

    @ApiModelProperty(notes = "The database generated driver ID")
    @JsonIgnore
    private Long id;

    @ApiModelProperty(notes = "The driver name", example = "Stingo")
    @NotBlank(message = "{validation.driver.name.notBlank}")
    private String name;

    @ApiModelProperty(notes = "The driver surname", example = "Fung")
    @NotBlank(message = "{validation.driver.surname.notBlank}")
    private String surName;

    @ApiModelProperty(notes = "The driver type license", example = "A")
    @NotBlank(message = "{validation.driver.license.notBlank}")
    @Size(min = 1, max = 1, message = "{validation.driver.license.size}")
    private String license;

    public DriverDTO() {

    }

    public DriverDTO(Driver driver) {
        this.id = driver.getId();
        this.name = driver.getName();
        this.surName = driver.getSurName();
        this.license = driver.getLicense();
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }
    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getLicense() {
        return license;
    }
    public void setLicense(String license) {
        this.license = license;
    }

    @Override
    public String toString() {
        return "{\"name\":" + name + ", \"surName\":" + surName + ", \"license\":" + license + "}";
    }

}