package com.hiberus.uster.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hiberus.uster.entity.Trip;
import com.hiberus.uster.util.CheckDateFormat;

import com.hiberus.uster.util.LocalDateDeserializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.*;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

@ApiModel(description = "All details about the Trip")
public class TripDTO {

    @ApiModelProperty(notes = "The database generated trip ID")
    @JsonIgnore
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("date")
    @ApiModelProperty(notes = "The trip date", dataType = "java.lang.String", example = "yyyy-MM-dd")
    //@NotBlank(message = "{validation.trip.date.notBlank}")
    //@CheckDateFormat(pattern = "yyyy-MM-dd")
    //@JsonDeserialize(using = LocalDateDeserializer.class)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate date;

    @ApiModelProperty(notes = "The trip vehicleID")
    @NotNull(message = "{validation.trip.vehicleId.notBlank}")
    @Digits(integer = 10, fraction = 0, message = "{validation.trip.vehicleId.notBlank}")
    private Long vehicleId;

    @ApiModelProperty(notes = "The trip driverID")
    @NotNull(message = "{validation.trip.driverId.notBlank}")
    @Digits(integer = 10, fraction = 0, message = "{validation.trip.driverId.notBlank}")
    private Long driverId;

    public TripDTO(){

    }

    public TripDTO(Trip trip) {
        this.id = trip.getId();
        this.date = trip.getDate();
        this.vehicleId = trip.getVehicle().getId();
        this.driverId = trip.getDriver().getId();
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getVehicleId() {
        return vehicleId;
    }
    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Long getDriverId() {
        return driverId;
    }
    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    @Override
    public String toString() {
        return "{\"vehicleId\":" + vehicleId + ", \"driverId\":" + driverId + ", \"date\":" + date + "}";
    }

}