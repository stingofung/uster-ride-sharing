package com.hiberus.uster.service;

import com.hiberus.uster.dto.VehicleDTO;
import com.hiberus.uster.dto.VehicleResponseDTO;
import com.hiberus.uster.entity.Vehicle;
import com.hiberus.uster.repository.TripRepository;
import com.hiberus.uster.repository.VehicleRepository;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class VehicleService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private TripRepository tripRepository;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public List<VehicleResponseDTO> findAll() {
        List<VehicleResponseDTO> list = vehicleRepository.findAll().stream()
                .map(entity -> new VehicleResponseDTO(entity))
                .collect(Collectors.toList());
        log.info("[VehicleService (API REST)] List of Vehicles consulted. {" + list.size() + "} record(s) returned.");
        return list;
    }

    public VehicleDTO findById(Long id) {
        Vehicle vehicle = findOneSafe(id).get();
        log.info("[VehicleService (API REST)] Vehicle consulted with {id:" + id + "}");
        return new VehicleDTO(vehicle);
    }

    @Transactional
    public VehicleDTO create(VehicleDTO vehicleDTO) {
        Vehicle vehicle = new Vehicle();
        vehicle.setBrand(vehicleDTO.getBrand());
        vehicle.setModel(vehicleDTO.getModel());
        vehicle.setPlate(vehicleDTO.getPlate());
        vehicle.setLicense(vehicleDTO.getLicense());

        Vehicle savedVehicle = vehicleRepository.saveAndFlush(vehicle);
        log.info("[VehicleService (API REST)] New Vehicle added with {id:" + savedVehicle.getId() + "}");
        return new VehicleDTO(savedVehicle);
    }

    @Transactional
    public VehicleDTO update(Long id, VehicleDTO vehicleDTO) {
        Vehicle vehicle = findOneSafe(id).get();
        vehicle.setBrand(vehicleDTO.getBrand());
        vehicle.setModel(vehicleDTO.getModel());
        vehicle.setPlate(vehicleDTO.getPlate());
        vehicle.setLicense(vehicleDTO.getLicense());
        log.info("[VehicleService (API REST)] Vehicle updated with {id:" + vehicle.getId() + "}");
        return new VehicleDTO(vehicle);
    }

    @Transactional
    public void delete(Long id) {
        Vehicle vehicle = findOneSafe(id).get();
        vehicleRepository.delete(vehicle);
        log.info("[VehicleService (API REST)] Vehicle deleted with {id:" + id + "}");
    }

    public Optional<Vehicle> findOneSafe(Long id) {
        return vehicleRepository.findById(id);
    }

	public List<VehicleResponseDTO> findAllByDate(String date) {
        LocalDate tripDate = LocalDate.parse(date, formatter);
        final List<Vehicle> availableVehicles = vehicleRepository.findAllByTripDateNot(tripDate);
        log.info("[VehicleService (API REST)] List of Available Vehicles consulted. {" + availableVehicles.size() + "} record(s) returned.");
        return availableVehicles.stream()
                .map(VehicleResponseDTO::new).distinct()
                .collect(Collectors.toList());
	}

}