package com.hiberus.uster.service;

import com.hiberus.uster.dto.DriverDTO;
import com.hiberus.uster.dto.DriverResponseDTO;
import com.hiberus.uster.entity.Driver;
import com.hiberus.uster.repository.DriverRepository;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DriverService {

    @Autowired
    private DriverRepository driverRepository;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public List<DriverResponseDTO> findAll() {
        List<DriverResponseDTO> list = driverRepository.findAll().stream()
                .map(entity -> new DriverResponseDTO(entity))
                .collect(Collectors.toList());
        log.info("[DriverService (API REST)] List of Drivers consulted. {" + list.size() + "} record(s) returned.");
        return list;
    }

    public DriverDTO findById(Long id) {
        Driver driver = findOneSafe(id).get();
        log.info("[DriverService (API REST)] Driver consulted with {id:" + id + "}");
        return new DriverDTO(driver);
    }

    @Transactional
    public DriverDTO create(DriverDTO driverDTO) {
        Driver driver = new Driver();
        driver.setName(driverDTO.getName());
        driver.setSurName(driverDTO.getSurName());
        driver.setLicense(driverDTO.getLicense());

        Driver savedDriver = driverRepository.saveAndFlush(driver);
        log.info("[DriverService (API REST)] New Driver added with {id:" + savedDriver.getId() + "}");
        return new DriverDTO(savedDriver);
    }

    @Transactional
    public Driver update(Driver driver, DriverDTO driverDTO) {
        driver.setName(driverDTO.getName());
        driver.setSurName(driverDTO.getSurName());
        driver.setLicense(driverDTO.getLicense());
        log.info("[DriverService (API REST)] Driver updated with {id:" + driver.getId() + "}");
        return driverRepository.save(driver);
    }

    @Transactional
    public void delete(Long id) {
        Driver driver = findOneSafe(id).get();
        driverRepository.delete(driver);
        log.info("[DriverService (API REST)] Driver deleted with {id:" + id + "}");
    }

    public Optional<Driver> findOneSafe(Long id) {
        return driverRepository.findById(id);
    }

    public List<DriverResponseDTO> findAllByTripDateAndRequiredLicense(String date, String requiredLicense) {
        LocalDate tripDate = LocalDate.parse(date, formatter);
        final List<Driver> availableDrivers = driverRepository.findAllByTripDateAndRequiredLicenseNot(tripDate, requiredLicense);
        log.info("[DriverService (API REST)] List of Available Drivers consulted. {" + availableDrivers.size() + "} record(s) returned.");
        return availableDrivers.stream()
                .map(DriverResponseDTO::new).collect(Collectors.toList());
    }

}