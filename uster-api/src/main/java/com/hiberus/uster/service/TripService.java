package com.hiberus.uster.service;

import com.hiberus.uster.dto.TripDTO;
import com.hiberus.uster.dto.TripResponseDTO;
import com.hiberus.uster.entity.Driver;
import com.hiberus.uster.entity.Trip;
import com.hiberus.uster.entity.Vehicle;
import com.hiberus.uster.repository.TripRepository;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TripService {

    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private DriverService driverService;

    public List<TripDTO> findAll() {
        List<TripDTO> list = tripRepository.findAll().stream()
                .map(entity -> new TripDTO(entity))
                .collect(Collectors.toList());
        log.info("[TripService (API REST)] List of Trips consulted. {" + list.size() + "} record(s) returned.");
        return list;
    }

    public List<TripResponseDTO> findAllFullView() {
        List<TripResponseDTO> list = tripRepository.findAll().stream()
                .map(TripResponseDTO::fromTrip)
                .collect(Collectors.toList());
        log.info("[TripService (API REST)] Full List of Trips consulted. {" + list.size() + "} record(s) returned.");
        return list;
    }

    @Transactional
    public TripDTO create(TripDTO tripDTO) {
        Trip trip = new Trip();
        trip.setDate(tripDTO.getDate());

        Driver driver = driverService.findOneSafe(tripDTO.getDriverId()).get();
        trip.setDriver(driver);

        Vehicle vehicle = vehicleService.findOneSafe(tripDTO.getVehicleId()).get();
        trip.setVehicle(vehicle);

        Trip savedTrip = tripRepository.saveAndFlush(trip);
        log.info("[TripService (API REST)] New Trip added with {id:" + savedTrip.getId() + "}");
        return new TripDTO(savedTrip);
    }

}