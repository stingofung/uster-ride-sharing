package com.hiberus.uster.repository;

import com.hiberus.uster.entity.Driver;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface DriverRepository extends JpaRepository<Driver, Long> {

    @Query("FROM Driver WHERE UPPER(name) = UPPER(?1) AND UPPER(surName) = UPPER(?2) AND UPPER(license) = UPPER(?3)")
    Optional<Driver> findByNameAndSurNameAndLicense(String name, String surName, String license);

    @Query("SELECT d FROM Driver d WHERE (d.id NOT IN (SELECT t.driver FROM Trip t WHERE t.date = :date) AND d.license = :requiredLicense)")
    List<Driver> findAllByTripDateAndRequiredLicenseNot(@Param("date") LocalDate date, @Param("requiredLicense") String requiredLicense);

}