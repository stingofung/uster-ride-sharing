package com.hiberus.uster.repository;

import com.hiberus.uster.entity.Vehicle;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

     @Query("FROM Vehicle WHERE UPPER(brand) = UPPER(?1) AND UPPER(model) = UPPER(?2) AND UPPER(plate) = UPPER(?3) AND UPPER(license) = UPPER(?4)")
     Optional<Vehicle> findByBrandAndModelAndPlateAndLicense(String brand, String model, String plate, String license);

     @Query("SELECT v FROM Vehicle v WHERE v.id NOT IN (SELECT t.vehicle FROM Trip t WHERE t.date=:date)")
     List<Vehicle> findAllByTripDateNot(@Param("date") LocalDate date);

}