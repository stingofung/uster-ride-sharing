package com.hiberus.uster.repository;

import com.hiberus.uster.entity.Trip;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TripRepository extends JpaRepository<Trip, Long> {

    List<Trip> findAllByDate(final LocalDate date);

    @Query("FROM Trip WHERE date = UPPER(?1) AND vehicle = UPPER(?2) AND driver = UPPER(?3)")
    Optional<Trip> findByDateAndVehicleAndDriver(LocalDate date, Long vehicleId, Long driverId);

}