package com.hiberus.uster.controller;

import com.hiberus.uster.controller.util.BaseController;
import com.hiberus.uster.dto.TripDTO;
import com.hiberus.uster.dto.TripResponseDTO;
import com.hiberus.uster.entity.Driver;
import com.hiberus.uster.entity.Vehicle;
import com.hiberus.uster.service.DriverService;
import com.hiberus.uster.service.TripService;
import com.hiberus.uster.service.VehicleService;
import com.hiberus.uster.validation.TripValidator;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1")
public class TripController extends BaseController {

    @Autowired
    private TripService tripService;
    private TripValidator tripValidator;

    @Autowired
    private DriverService driverService;

    @Autowired
    private VehicleService vehicleService;

    @GetMapping("/trips")
    public List<TripDTO> getAllTrips() {
        log.info("[TripController (API REST)] GET - Request received: /trips");
        return tripService.findAll();
    }

    @GetMapping("/trips/full-view")
    public List<TripResponseDTO> getAllTripsFullView() {
        log.info("[TripController (API REST)] GET - Request received: /trips/full-view");
        return tripService.findAllFullView();
    }

    @PostMapping("/trips/add")
    public TripDTO addTrip(@Valid @RequestBody TripDTO tripDTO, BindingResult bindingResult) {
        log.info("[TripController (API REST)] POST - Request received: /trips/add -> " + tripDTO.toString());
        Driver driver = get(driverService.findOneSafe(tripDTO.getDriverId()),"validation.driver.notFound");
        Vehicle vehicle = get(vehicleService.findOneSafe(tripDTO.getVehicleId()),"validation.vehicle.notFound");
        checkRequest(tripDTO, tripValidator, bindingResult);
        return tripService.create(tripDTO);
    }

}