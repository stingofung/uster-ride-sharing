package com.hiberus.uster.controller.util;

import com.hiberus.uster.exception.BadRequestException;
import com.hiberus.uster.exception.NotFoundException;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import java.util.Optional;

public class BaseController {

    protected <T extends Object> T get(Optional<T> item) {
        return item.orElseThrow(() -> new NotFoundException());
    }

    protected <T extends Object> T get(Optional<T> item, String code) {
        return item.orElseThrow(() -> new NotFoundException(code));
    }

    protected void checkRequest(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new BadRequestException(bindingResult);
        }
    }

    protected void checkRequest(Object object, Validator validator, BindingResult bindingResult) {
        checkRequest(bindingResult);
        validator.validate(object, bindingResult);
        checkRequest(bindingResult);
    }

    protected ResponseEntity<Void> ok() {
        return ResponseEntity.ok().build();
    }

}