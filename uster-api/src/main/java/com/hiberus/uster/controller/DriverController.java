package com.hiberus.uster.controller;

import com.hiberus.uster.controller.util.BaseController;
import com.hiberus.uster.dto.DriverDTO;
import com.hiberus.uster.dto.DriverResponseDTO;
import com.hiberus.uster.entity.Driver;
import com.hiberus.uster.exception.DriverNotFoundException;
import com.hiberus.uster.service.DriverService;
import com.hiberus.uster.validation.DriverValidator;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1")
public class DriverController extends BaseController {

    @Autowired
    private DriverService driverService;
    private DriverValidator driverValidator;

    @GetMapping("/drivers")
    public List<DriverResponseDTO> getAllDrivers() throws DriverNotFoundException {
        log.info("[DriverController (API REST)] GET - Request received: /drivers");
        return driverService.findAll();
    }

    @GetMapping("/drivers/{id}")
    public DriverDTO getDriverById(@PathVariable(value = "id") Long id) {
        log.info("[DriverController (API REST)] Request received: /drivers/" + id);
        return driverService.findById(id);
    }

    @GetMapping("/drivers/filterBy/date/{date}/license/{license}")
    public List<DriverResponseDTO> getDriversByAvailableDateAndVehicleLicenseType(@PathVariable(value = "date") String date, @PathVariable(value = "license") String license) {
        log.info("[DriverController (API REST)] GET - Request received: /drivers/filterBy/date/" + date + "/license/" + license);
        return driverService.findAllByTripDateAndRequiredLicense(date, license);
    }

    @PostMapping("/drivers/add")
    public DriverDTO addDriver(@Valid @RequestBody DriverDTO driverDTO, BindingResult bindingResult) {
        log.info("[DriverController (API REST)] POST - Request received: /drivers/add -> " + driverDTO.toString());
        checkRequest(driverDTO, driverValidator, bindingResult);
        return driverService.create(driverDTO);
    }

    @PutMapping("/drivers/{id}")
    public void updateDriver(@PathVariable(value = "id") Long id, @Valid @RequestBody DriverDTO driverDTO, BindingResult bindingResult) {
        log.info("[DriverController (API REST)] PUT - Request received: /drivers/" + id + " -> " + driverDTO.toString());
        Driver driver = get(driverService.findOneSafe(id),"validation.driver.notFound");
        driverDTO.setId(id);
        checkRequest(driverDTO, driverValidator, bindingResult);
        driverService.update(driver, driverDTO);
    }

    @DeleteMapping("/drivers/{id}")
    public void deleteDriver(@PathVariable(value = "id") Long id) {
        log.info("[DriverController (API REST)] DELETE - Request received: /drivers/" + id);
        Driver driver = get(driverService.findOneSafe(id),"validation.driver.notFound");
        driverService.delete(id);
    }

}