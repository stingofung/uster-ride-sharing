package com.hiberus.uster.controller.util;

import com.hiberus.uster.dto.util.JsonErrorDTO;
import com.hiberus.uster.exception.BadRequestException;
import com.hiberus.uster.exception.NotFoundException;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RestControllerAdvice
public class ErrorHandlerController {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> notFound(NotFoundException e, HttpServletRequest request) {
        String message = (e.hasCode()) ? messageSource.getMessage(e.getMessage(), null, LocaleContextHolder.getLocale()) : "";
        log.error("NotFound for: {}\nCause: {}", request.getRequestURI(), message);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<JsonErrorDTO> badRequest(BadRequestException e, HttpServletRequest request) {
        JsonErrorDTO response = new JsonErrorDTO();

        List<FieldError> errorList = e.getErrors().getFieldErrors();

        for (FieldError item : errorList) {
            String message = messageSource.getMessage(item, LocaleContextHolder.getLocale());
            response.addError(item.getField(), message);
        }

        log.error("Bad request for: {}\nCause: {}", request.getRequestURI(), response.getErrors().get(0).getMessage());
        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<JsonErrorDTO> notReadableException(HttpMessageNotReadableException e, HttpServletRequest request) {
        String error = e.getMessage();
        log.error("Bad request for {}\nCause: {}", request.getRequestURI(), error);
        return ResponseEntity.badRequest().body(new JsonErrorDTO(error));
    }

    @ExceptionHandler(IncorrectResultSizeDataAccessException.class)
    public ResponseEntity<JsonErrorDTO> incorrectResultSize(IncorrectResultSizeDataAccessException e) {
        String error = e.getMessage();
        log.error(error, e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonErrorDTO(error));
    }

    @ExceptionHandler(NoClassDefFoundError.class)
    public void logError(NoClassDefFoundError e) {
        StackTraceElement[] items = e.getStackTrace();
        Stream.of(items).skip(2).collect(Collectors.toList()).forEach(item -> {
            log.error("error: ", item);
        });
    }

}