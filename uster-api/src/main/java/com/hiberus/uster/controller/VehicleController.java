package com.hiberus.uster.controller;

import com.hiberus.uster.controller.util.BaseController;
import com.hiberus.uster.dto.VehicleDTO;
import com.hiberus.uster.dto.VehicleResponseDTO;
import com.hiberus.uster.entity.Vehicle;
import com.hiberus.uster.service.VehicleService;
import com.hiberus.uster.validation.VehicleValidator;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1")
public class VehicleController extends BaseController {

    @Autowired
    private VehicleService vehicleService;
    private VehicleValidator vehicleValidator;

    @GetMapping("/vehicles")
    public List<VehicleResponseDTO> getAllVehicles() {
        log.info("[VehicleController (API REST)] GET - Request received: /vehicles");
        return vehicleService.findAll();
    }

    @GetMapping("/vehicles/{id}")
    public VehicleDTO getVehicleById(@PathVariable(value = "id") Long id) {
        log.info("[VehicleController (API REST)] Request received: /vehicles/" + id);
        return vehicleService.findById(id);
    }

    @GetMapping("/vehicles/filterBy/date/{date}")
    public List<VehicleResponseDTO> getVehiclesByDate(@PathVariable(value = "date") String date) {
        log.info("[VehicleController (API REST)] GET - Request received: /vehicles/filterBy/date/" + date);
    	return vehicleService.findAllByDate(date);
    }

    @PostMapping("/vehicles/add")
    public VehicleDTO addVehicle(@Valid @RequestBody VehicleDTO vehicleDTO, BindingResult bindingResult) {
        log.info("[VehicleController (API REST)] POST - Request received: /vehicles/add -> " + vehicleDTO.toString());
        checkRequest(vehicleDTO, vehicleValidator, bindingResult);
        return vehicleService.create(vehicleDTO);
    }

    @PutMapping("/vehicles/{id}")
    public VehicleDTO updateVehicle(@PathVariable(value = "id") Long id, @Valid @RequestBody VehicleDTO vehicleDTO, BindingResult bindingResult) {
        log.info("[VehicleController (API REST)] PUT - Request received: /vehicles/" + id + " -> " + vehicleDTO.toString());
        Vehicle vehicle = get(vehicleService.findOneSafe(id),"validation.vehicle.notFound");
        vehicleDTO.setId(id);
        checkRequest(vehicleDTO, vehicleValidator, bindingResult);
        return vehicleService.update(id, vehicleDTO);
    }

    @DeleteMapping("/vehicles/{id}")
    public void deleteVehicle(@PathVariable(value = "id") Long id) {
        log.info("[VehicleController (API REST)] PUT - Request received: /vehicles/" + id);
        Vehicle vehicle = get(vehicleService.findOneSafe(id),"validation.vehicle.notFound");
        vehicleService.delete(id);
    }

}