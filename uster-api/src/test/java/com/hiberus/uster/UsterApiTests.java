package com.hiberus.uster;

import static org.mockito.Mockito.*;

import static org.hamcrest.Matchers.containsString;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.hiberus.uster.dto.DriverResponseDTO;
import com.hiberus.uster.dto.TripDTO;
import com.hiberus.uster.dto.VehicleDTO;
import com.hiberus.uster.dto.VehicleResponseDTO;
import com.hiberus.uster.entity.Driver;
import com.hiberus.uster.entity.Trip;
import com.hiberus.uster.entity.Vehicle;
import com.hiberus.uster.service.DriverService;
import com.hiberus.uster.service.TripService;
import com.hiberus.uster.service.VehicleService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class UsterApiTests {

	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private DriverService driverService;
	@MockBean
	private TripService tripService;
	@MockBean
	private VehicleService vehicleService;

	@Test
	public void driverEndpointTest() throws Exception {
		final Driver driver = new Driver();
		driver.setId(1L);
		driver.setName("Stingo");
		driver.setSurName("Fung");
		driver.setLicense("A");

		when(driverService.findAll()).thenReturn(Collections.singletonList(new DriverResponseDTO(driver)));

		/*this.mockMvc.perform(get("/api/v1/drivers")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("\"id\":1,\"name\":\"Stingo\",\"surName\":\"Fung\",\"license\":\"A\"")));*/
		this.mockMvc.perform(get("/api/v1/drivers")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("\"name\":\"Stingo\",\"surName\":\"Fung\",\"license\":\"A\"")));
	}

	@Test
	public void tripEndpointTest() throws Exception {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		final LocalDate tripDate = LocalDate.parse("2020-02-26", formatter);
		final Trip trip = new Trip();
		final Vehicle vehicle = new Vehicle();
		final Driver driver = new Driver();
		trip.setId(1L);
		vehicle.setId(2L);
		driver.setId(1L);
		trip.setDate(tripDate);
		trip.setVehicle(vehicle);
		trip.setDriver(driver);

		when(tripService.findAll()).thenReturn(Collections.singletonList(new TripDTO(trip)));

		this.mockMvc.perform(get("/api/v1/trips")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("\"id\":1,\"vehicleId\":2,\"driverId\":1,\"date\":\"2020-02-26\"")));
	}

	@Test
	public void vehicleEndpointTest() throws Exception {
		final Vehicle vehicle = new Vehicle();
		vehicle.setId(1L);
		vehicle.setBrand("Chevrolet");
		vehicle.setModel("Camaro");
		vehicle.setPlate("ABC-1234");
		vehicle.setLicense("C");

		when(vehicleService.findAll()).thenReturn(Collections.singletonList(new VehicleResponseDTO(vehicle)));

		this.mockMvc.perform(get("/api/v1/vehicles")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("\"id\":1,\"brand\":\"Chevrolet\",\"model\":\"Camaro\",\"plate\":\"ABC-1234\",\"license\":\"C\"")));
	}

}